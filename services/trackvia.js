const TrackViaSDK = require('trackvia-node-sdk')

/**
 * Encapsulation class for the TrackVia Node SDK.
 */
class TrackVia {
    constructor(authentication, environment, accountId) {
        this.sdk = new TrackViaSDK(authentication.apiKey, 
            authentication.accessToken, 
            environment.url,
            accountId)
    }

    /**
     * 
     * @param {Int} viewId 
     * @param {Int} recordId
     * @param {Object} callback  
     * @returns {Promise<Object>} response
     */
     async getRecord(viewId, recordId, callback) {
        try {
            const response = await this.sdk.getRecord(viewId, recordId)
            return response
        }
        catch (err) {
            console.error(`[ERROR] Could not fetch record with id '${recordId}'. Error: ${err}.`)
            callback(null, err)
            return null
        }
    }

    /**
     * 
     * @param {Int} viewId 
     * @param {Object} data 
     * @returns {Promise<Object>} response
     */
    async addRecord(viewId, data, callback) {
        try {
            const response = await this.sdk.addRecord(viewId, data)
            return response
        }
        catch (err) {
            console.error("[ERROR] Could not create record. Error: ", err)
            callback(null, err)
            return null
        }
    }

    /**
     * 
     * @param {Int} viewId 
     * @param {Int} recordId 
     * @param {Object} fileObject 
     * @returns {Promise<Object>} response
     */
    async attachFileData(viewId, recordId, fieldName, fileData, filename, callback) {
        try {
            const response = await this.sdk.attachFileData(viewId, recordId, fieldName, fileData, filename)
            return response
        }
        catch (err) {
            console.error(`[ERROR] Failed to attach QR Code Image to record. Error: '${err}'.`)
            callback(null, err)
            return null
        }
    }
}
module.exports = TrackVia;