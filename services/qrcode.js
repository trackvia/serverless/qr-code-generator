const QRCode = require('qrcode')

/**
 * Given a string, generate a QR Code PNG.
 * 
 * @async
 * @function generate
 * @param {String} string Input string for code generation
 * @param {String} filename Optional string for filename. If not provided, uses generated value from input.
 * @returns {Promise<{Buffer, String, String}>} File data, file type, filename
 * 
 */
async function generate(string, filename = null, callback) {
    try {
        console.log(`[INFO] Using input value '${string}' for generating QR code...`)
        let base64 = await QRCode.toDataURL(string)
        const type = base64.split(';')[0].split('/')[1]
        const data = new Buffer.from(base64.replace(/^data:image\/\w+;base64,/, ""), 'base64')
        const name = filename ?? `qrcode-${string.substring(0, 16)}.${type}`
        console.log(`[INFO] Successfully generated QR code. Filename: '${name}' and type: '${type}'.`)
        return { data, type, filename: name }
    }
    catch (err) {
        const message = `[ERROR] An exception occurred while generating the barcode. Error: '${err}'.`
        console.error(message)
        callback(null, message) 
        return null
    }
}

/**
 * Given a string, stringify using JSON and remove edging 
 * double-quotes from the string if they exist after conversion.
 * 
 * @param {Object} input Input value for converting and cleaning.
 * @returns {String} The value with quotes removed.
 */
function clean(input) {
    return JSON.stringify(input).replace(/^"(.*)"$/, '$1')
}

module.exports = { generate, clean }