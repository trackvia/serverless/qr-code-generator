# QR Code Generator

## Description

Lambda service for taking a string and generating a QR code that can be uploaded to TrackVia.

## Usage

- Update `configuration/authentication.json` to change the access token and user key.
- Update `configuration/constants.json` to change parameters used for table, view, and image field name.
- Update `configuration/environment.json` to change the url used if not go.trackvia.com.

### Required Parameters

Defined in `constants.json`, the parameters available for configuration are:

- `accountId`: Account ID to execute the microservice.
- `fieldNameForQRCodeInput`: Field name on incoming record data for QR code input value. If not found, defaults to value for 'recordId' from microservice event input.
- `create.tableId`: Table containing record being updated with output QR code image.
- `create.viewId`: Table containing record being updated with output QR code image.
- `create.formId`: Form for saving updated QR code image on record.
- `create.fieldNameForQRCodeImageOutput`: Field to save QR code image.
- `create.filename`: Optional filename including extension, ex: `"MyBarcodeImage.png"`. Provide `null` to use default system-generated filename.

## Installation

```
git clone git@gitlab.com:trackvia/serverless/qr-code-generator.git
npm install
```

## Developing

Using SAM, Lambda Local, Serverless Framework locally or uploading package to AWS Lambda for cloud execution.

## Testing

```
npm test
```

## Support

https://gitlab.com/trackvia/serverless/qr-code-generator/-/issues


## License

This software is provided via the [ISC License](LICENSE.md).