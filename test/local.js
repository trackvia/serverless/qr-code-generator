// This file is used for testing lambdas locally on your machine
// without requiring any upload or connection to AWS.
//
// Starting Server:
// 
// lambda-local -l test/local.js -h handler --watch 8008
// 
// Calling Local Lambda:
// 
// curl --request POST \
//   --url http://localhost:8008/ \
//   --header 'content-type: application/json' \
//   --data '{
//         "event": {
//                 "key1": "value1",
//                 "key2": "value2",
//                 "key3": "value3"
//         }
// }'
const lambdaLocal = require('lambda-local');

// The payload intended to pass to the lambda event handler
const jsonPayload = { "recordId": 78 }

lambdaLocal.execute({
    event: jsonPayload,
    lambdaPath: './index.js',
    timeoutMs: 4000,
    // verboseLevel: 2
}).then(function(done) {
    console.log(done);
}).catch(function(err) {
    console.log(err);
});