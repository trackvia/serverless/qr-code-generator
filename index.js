// Template Imports
const authentication = require("./configuration/authentication.json")
const environment = require("./configuration/environment.json")
const constants = require("./configuration/constants.json")
const TrackVia = require('./services/trackvia.js')

// Custom Imports
const qrcode = require('./services/qrcode')

/**
 * ---------------------------------------------
 * Lambda Handler Function
 * 
 * Title: QR Code Generator
 * Description: Convert the input record identifier into QR Code image data and upload to same record at the identified file field name.
 * Author: Kyle Turner
 * Repository: https://gitlab.com/trackvia/serverless/qr-code-generator
 * ---------------------------------------------
 */
exports.handler = async function(event, context, callback) {
    console.log("[DEBUG] Handler incoming event data: ", event)

    // 0. Handler setup
    //
    // Only complete once callback blocks are executed
    context.callbackWaitsForEmptyEventLoop = false
    // Destination view and record (one which triggered this lambda) for attaching image
    const recordId = event.recordId
    const viewId = constants.create.viewId
    // Field name to attach image once code is generated
    const fieldNameForQRCodeImageOutput = constants.create.fieldNameForQRCodeImageOutput
    // See `configuration` folder for authentication and environment details.
    const trackvia = new TrackVia(authentication, environment, constants.accountId)

    // 1. Determine QR Code source input
    //
    // - If fieldNameForQRCodeInput present, fetch record and update source with field value.
    // - Otherwise, default to record id string value.
    let qrCodeInput = `${recordId}`
    if (constants.fieldNameForQRCodeInput) {
        const record = await trackvia.getRecord(constants.create.viewId, recordId, callback)
        // If field data exists on record, update input with value
        if (record && record.data && record.data[constants.fieldNameForQRCodeInput]) {
            // String type is required for generation input
            // Stringify and remove outer quotation marks, if applicable
            qrCodeInput = qrcode.clean(record.data[constants.fieldNameForQRCodeInput])
        }
    }
    // 2. Generate QR Code
    const fileOutput = await qrcode.generate(qrCodeInput, constants.create.filename, callback)

    // 3. Attach generated QR code to file field in TrackVia
    await trackvia.attachFileData(
        viewId, 
        recordId, 
        fieldNameForQRCodeImageOutput, 
        fileOutput.data, 
        fileOutput.filename, 
        callback)
    
    // 4. Notify Success
    callback(null, 'QR Code successfully generated.')
}